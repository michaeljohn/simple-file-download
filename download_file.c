/*
 * Download a file and calculate the SHA256.
 * Downloaded file saved as 'file.out' to keep the example simple.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <curl/curl.h>
#include <sodium.h>

crypto_hash_sha256_state sha_state;

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
	size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
	crypto_hash_sha256_update(&sha_state, (char *)ptr, nmemb);
	return written;
}

int main(int argc, char *argv[])
{
	unsigned char out[crypto_hash_sha256_BYTES];
	CURL *curl_handle;
	static const char *pagefilename = "file.out";
	FILE *pagefile;

	if (argc < 2) {
		printf("Missing arguments\n");
		return 1;
	}

	/* init sha256 state */
	crypto_hash_sha256_init(&sha_state);

	curl_global_init(CURL_GLOBAL_ALL);
	/* init the curl session */
	curl_handle = curl_easy_init();
	/* set URL to get here */
	curl_easy_setopt(curl_handle, CURLOPT_URL, argv[1]);
	/* switch on full protocol output while testing */
	curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
	/* disable progress meter, set to 0L to enable it */
	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
	/* send all data to this function */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
	/* open file */
	pagefile = fopen(pagefilename, "wb");
	if (pagefile) {
		/* write the page body to this file handle */
		curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
		curl_easy_perform(curl_handle);
		fclose(pagefile);
	}
	/* cleanup */
	curl_easy_cleanup(curl_handle);
	curl_global_cleanup();

	/* calculate the sha256 of the downloaded file */
	crypto_hash_sha256_final(&sha_state, out);
	char* hex = calloc(crypto_hash_sha256_BYTES * 2 + 1, sizeof(char));
	sodium_bin2hex(hex, crypto_hash_sha256_BYTES * 2 + 1, out, crypto_hash_sha256_BYTES);
	printf("%s\n", hex);

	return 0;
}

